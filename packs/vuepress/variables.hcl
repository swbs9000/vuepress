variable "job_name" {
  description = "The name to use as the job name which overrides using the pack name"
  type        = string
  // If "", the pack name will be used
  default = ""
}

variable "datacenters" {
  description = "A list of datacenters in the region which are eligible for task placement"
  type        = list(string)
  default     = ["dc1"]
}

variable "region" {
  description = "The region where the job should be placed"
  type        = string
  default     = "global"
}

variable "consul_service_name" {
  description = "The consul service you wish to load balance"
  type        = string
  default     = "webapp"
}

variable "version_tag" {
  description = "The docker image version. For options, see https://hub.docker.com/repository/docker/swbs90/vuepress"
  type        = string
  default     = "latest"
}

#variable "http_port" {
#  description = "The Nomad client port that routes to the Vuepress. This port will be where you visit your load balanced application"
#  type        = number
#  default     = 8000
#}

variable "resources" {
  description = "The resource to assign to the Vuepress system task that runs on every client"
  type = object({
    cpu    = number
    memory = number
  })
  default = {
    cpu    = 800,
    memory = 1200
  }
}
